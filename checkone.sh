#!/bin/bash
path=./bin/

arr_stream=(1024 1016 1012)

arr_index=0
echo 
i=0
while [ $i -le $2 ]
do
    echo "==>"$(printf "%02d" "$i")
    echo ./tcheck_x86.out ${path}t_stream_$1_$i.bin ${path}t_cnkner_$1_$i.bin ${path}t_dstream_$1_$i.bin ${arr_stream[i]} ${arr_stream[i]} 1
    ./tcheck_x86.out ${path}t_stream_$1_$i.bin ${path}t_cnkner_$1_$i.bin ${path}t_dstream_$1_$i.bin ${arr_stream[i]} ${arr_stream[i]} 1
    let "i++"
done