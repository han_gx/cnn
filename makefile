progName=a.out
CFLAGS=-g
# CFLAGS=
CC=arm-linux-gnueabihf-gcc
ARCH=ARM
PLAT_FORM_ARM=ARM
PLAT_FORM_X86=X86

PARA= -O3
# PARA=
DEBUG=
COMPILE_TYPE=

ifeq ($(COMPILE_TYPE), SHARE)
PARA += -shared -fPIC
progName = libJsztCNN.so
endif
ifeq ($(DEBUG), DEBUG)
PARA += -DDEBUG
endif
ifeq ($(ARCH), $(PLAT_FORM_ARM))
PARA += -DPLATFORM_ARM
endif
ifeq ($(ARCH), $(PLAT_FORM_X86))
PARA += -DPLATFORM_X86
CC=gcc
endif

INCLUDES=./include ./src
SUBDIR=./src ./include
wildcard_search=$(foreach x, $(SUBDIR), $(x)/*.c)
sources += $(wildcard $(wildcard_search))
# sour_d += $(sources:.c=.d)

# objects += $(patsubst %.c,%.o,$(wildcard src/*.c include/*.c))
objects += $(patsubst %.c,%.o,$(sources))

run:	$(objects)
	$(CC) $(PARA) $(CFLAGS) -o $(progName) -w -D_FILE_OFFSET_BITS=64 -D_LARGE_FILE -pthread -lm $(objects) -Wno-incompatible-pointer-types

$(objects):	%.o: 	%.c
	$(CC) $(PARA) -c $(CFLAGS) $< -o $@ $(addprefix -I, $(INCLUDES))
# $(CC) -MM $(CFLAGS) $< $(addprefix -I, $(INCLUDES)) > $*.d

# -include $(sour_d)
# src/main.o: src/main.c include/common.h
# src/main.o src/main.d : src/main.c include/common.h

test:  
	echo $(sources)
	# echo $(sour_d)
	# echo $(objects)

test1:
	sed -e "1 i 123" src/AB_init.d
	
.PHONY:	clean

clean:
	-rm -rf $(objects)
	-rm -rf $(sour_d)
	-rm -rf $(progName)
	-rm -rf ./*.o
	-rm -rf ./src/*.d.*

cleanAll: 
	-rm -rf $(objects)
	-rm -rf $(progName)
