#include <stdio.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <string.h>
#include <errno.h>

typedef struct
{
    unsigned char fname[100];   //文件
    int offset;     //偏移
    int row_nums;   //一行显示多少字节
    int rows;       //显示多少行
} HEX_stu;
HEX_stu hex_stu;

void start() {
    int fd = open(&hex_stu.fname, O_RDONLY);

    if(fd <= 0) {
        printf("Error open file, err code: %s \n", strerror(errno));
        exit(0);
    }

    // int fsize = 

}

void encap_para(unsigned char type, unsigned char *ptr) {
    printf("%c -> %s\n", type, ptr);
    int ret = 0;
    switch (type)
    {
    case 'f':
        memcpy(&hex_stu.fname, ptr, strlen(ptr));
        break;
    
    case 's':
        hex_stu.offset = atoi(ptr);
        break;

    case 'n':
        hex_stu.row_nums = atoi(ptr);
        break;

    case 'c':
        hex_stu.rows = atoi(ptr);
        break; 

    default:
        printf("Error paramemter, unknow type -%c \n", type);
        exit(0);
    }
}

int main(int argc, char const *argv[])
{

    if(argc % 2 == 0) {
        printf("Error parameter \n");
        printf("hex -f xx.bin -s 0 -n 1024 -c 1000 \n");
        printf("    -f file name \n");
        printf("    -s offset \n");
        printf("    -n one row nums, default 16 byte \n");
        printf("    -c show many rows, default all \n");
        exit(0);
    }

    memset(&hex_stu, 0x0, sizeof(HEX_stu));

    int i = 0;
    for(i = 1; i < argc; i+=2) {
        if(argv[i][0] == '-') {
            encap_para(argv[i][1], argv[i+1]);
        } else {
            printf("Error to analysis parameter, %s \n", argv[i][0]);
            exit(0);
        }
    }

    start();
    
    return 0;
}
