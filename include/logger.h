#ifndef __LOGGER_H_H
#define __LOGGER_H_H
#include "common.h"
/****
 * 
 *  logger modol 
 * 
 */
#define     _GRAY           "\033[5;0;30m" 
#define     _READ           "\033[5;0;31m" 
#define     _GREEN_         "\033[5;0;32m"
#define     _YELLOW         "\033[5;0;33m" 
#define     _BLUE           "\033[5;0;34m" 
#define     _PURPLE         "\033[5;0;35m"
#define     _DARKGREN       "\033[5;0;36m"
#define     _WHITE          "\033[5;0;37m"
#define     _DEFAULT        "\033[5;0;0m"
#define     _COREND         "\033[5m"   
#define     _F_NAME_LEN     10
#define     _FUN_NAME_LEN   5
#define     __NEW_LINE      "\n"
// #define     __NEW_LINE      ""
// 获取时间
uint8 *__GET_LOCAL_TIME();
#define ___GET_LOCAL_TIME __GET_LOCAL_TIME()
#define     PRINT(...)         printf(__VA_ARGS__)
// DEBUG 模式
#ifdef      _LOG_DEBUG_
#define     LOG_DEBUG(format, ...)      printf(_BLUE  "%s" _BLUE  " [DEBUG] " _PURPLE "%*s:" _DARKGREN "%-3d " _YELLOW "%-*s: \033[0m" format ""__NEW_LINE"", ___GET_LOCAL_TIME, _F_NAME_LEN, __FILE__, __LINE__, _FUN_NAME_LEN, __FUNCTION__, ##__VA_ARGS__)
#else
#define     LOG_DEBUG(...)
#endif

// INFO 模式

#if         _LOG_LEVEL_
#define     LOG_INFO(format, ...)       printf(_GREEN_"%s" _GREEN_ " [INFO ] " _PURPLE "%*s:" _DARKGREN "%-3d " _YELLOW "%-*s: \033[0m" format ""__NEW_LINE"", ___GET_LOCAL_TIME, _F_NAME_LEN, __FILE__, __LINE__, _FUN_NAME_LEN, __FUNCTION__, ##__VA_ARGS__)
#define     LOG_WARN(format, ...)       printf(_GRAY  "%s" _GRAY   " [WARN ] " _PURPLE "%*s:" _DARKGREN "%-3d " _YELLOW "%-*s: \033[0m" format ""__NEW_LINE"", ___GET_LOCAL_TIME, _F_NAME_LEN, __FILE__, __LINE__, _FUN_NAME_LEN, __FUNCTION__, ##__VA_ARGS__)
#define     LOG_ERROR(format, ...)      printf(_READ  "%s" _READ   " [ERROR] " _PURPLE "%*s:" _DARKGREN "%-3d " _YELLOW "%-*s: \033[0m" _READ format "\033[0m"__NEW_LINE"", ___GET_LOCAL_TIME, _F_NAME_LEN, __FILE__, __LINE__, _FUN_NAME_LEN, __FUNCTION__, ##__VA_ARGS__)

#endif


/**测试*/
// #define     LOG_DEBUG(...) 
// #define     LOG_INFO(...)  
// #define     LOG_WARN(...)  
// #define     LOG_ERROR(...) 

/** 开启内存检测 */
#define     LOG_EXAM_TEST(...)
// /** 开启内存检测 */
// #define     LOG_EXAM_TEST(...)      printf(__VA_ARGS__)

void logger_test();

#endif  //__LOGGER_H_H