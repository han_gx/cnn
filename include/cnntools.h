#ifndef _JS_CNNTOOLS___H
#define _JS_CNNTOOLS___H

#include "jszt_cnn.h"

#define     DEBUG_RUNCNN   LOG_DEBUG

#define     CNNTEMP_LEN    1040*1040

typedef enum {
    JSCNN_AWAIT,    //线程已经启动, 等待计算
    JSCNN_START,    //接收到START, 开启计算
    JSCNN___END,    //接收到改指令, 各个线程结束, 并释放对应资源
} CNN_status;

typedef enum {
    TH_READY,       //就绪状态
    TH_OVER,        //结束运行
} TH_status;

typedef struct
{
    pthread_t thread_id;
    int total_times;
    int thread_index;
    volatile int status;
} TH_STATUS;    //线程的状态

typedef struct
{  
    CNN_PARA * cPara;
    volatile CNN_status status;         
    TH_STATUS *tstatus;
    volatile int cnn_times;
    int thread_total;
    int max_cpus;
    int wind_rows;      //平均需要处理的高度
    int last_rows;      //多余的行
    int cal_times;      //总共计算卷积的次数
    uint8 * cnnTemp;
} Golad_CNN;

typedef struct
{
    uint8 * stream;
    uint8 * dstream;
    uint32  height;
    uint32  width;
    uint8 * cnnker;
    uint8   ckerlen;
} CNN_ONE;


extern JSZT_CODE cnn_setEnd();
extern JSZT_CODE cnn_setReady();
JSZT_CODE init_CNN(int cur_cpus, int max_cpus);
extern JSZT_CODE manage_CNN(const CNN_PARA *cPara);
extern JSZT_CODE do_calculat_cpu(const CNN_PARA *cPara);
extern JSZT_CODE do_calculat_fpga(const CNN_PARA *cPara);

extern void printStream(const CNN_PARA *cnnPara);
extern void printDStream(const CNN_PARA *cnnPara);
extern void printCnnker(const CNN_PARA *cnnPara);
extern void writeAll(const CNN_PARA *cnnPara);
void writeWithIndex(const CNN_PARA *cnnPara, int index);
void writeWithCNNOne(const CNN_PARA *cnnPara, int times, int index);
#endif // !_JS_CNNTOOLS___H
