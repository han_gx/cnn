#ifndef __COMMON_H__H
#define __COMMON_H__H
#define _GNU_SOURCE

// 基础库引用
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>

// 数学计算相关库
#include <math.h>

// 多线程开发相关库
#include <pthread.h>
#include <sys/prctl.h>

// 信号量控制按键中断
#include <signal.h>

// 错误库文件
#include <errno.h>

// 网络库包应用
#include <sys/socket.h>
#include <arpa/inet.h>

//文件操作相关
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>

// 系统属性相关
#include <sys/sysinfo.h>
#include <sched.h>

#include "config.h"
#ifdef PLATFORM_X86
/** 测试模式,测试本地程序 */
#define         MODULE_TEST          1
#endif

#ifdef PLATFORM_ARM
#define         MODULE_TEST          0
#endif // DEBUG

/** 返回参数 **/
#define KT_OK		 				 0
#define KT_ERROR					-1
#define KT_WARN						-2

/** 无符号字节类型表示 **/
#define uint8           unsigned char
#define uint16          unsigned short
#define uint32          unsigned int
#define uint64          unsigned long

#define START_PROGRAM                0x00             
#define END___PROGRAM                0x01             

#define START_SCAN                   0x00             
#define END___SCAN                   0x01             

void        printHex(uint8 * rbuf, int len, char *name);
void        printHex1(uint8 * rbuf, int len, int uint, char *name);
/** 测试修改 .h 增量编译 */
// #define    COMPILE_INCRE

#include "logger.h"

/** 内存管理 */
#define   MM_MAX_TIME       10
#define   MM_MAX_SIZE       1024*MM_MAX_TIME

void js_mm_end();
int js_free(void * ptr);
void * js_malloc(int size);
int js_mm_init(int max_cnt, int max_size);

#endif // __COMMON_H__H