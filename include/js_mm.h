#ifndef __JS_MM___H
#define __JS_MM___H

#include "common.h"



#define MM_DEBUG  0
#if MM_DEBUG
#define LOG_MM_DEBUG    LOG_DEBUG
#else
#define LOG_MM_DEBUG
#endif // DEBUG

enum {
    MM_OK,
    MM_FAILE,
};

enum {
    IS_FREE,
    IS_USED,
};

typedef struct {
    int size;
    int *addr;
    uint8 flag; 
} MM_LO;

typedef struct
{
    int mm_cnt;                 //申请到的内存次数
    int max_cnt;
    int max_size;                
    MM_LO *mm_lo;
} JS_MM;

#endif // DEBUG