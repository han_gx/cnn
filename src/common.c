#include "common.h"
void printHex(uint8 * rbuf, int len, char *name) {
    int cnt = 0;
    for(cnt = 0; cnt < len; cnt++) {
        if(cnt%16 == 0) {
            printf("\n");
        }
        printf("%2d:%02x ", cnt, rbuf[cnt]);
    }
    printf("\n");
}

void printHex1(uint8 * rbuf, int len, int uint, char *name) {
    int cnt = 0;
    int index = 0;
    printf("[==>%s]", name);
    for(cnt = 0; cnt < len; cnt++) {
        if(cnt%uint == 0) {
            index++;
            printf("\n ==> %02d: ", index);
        }
        printf("%02x ", rbuf[cnt]);
    }
    printf("\n");
}