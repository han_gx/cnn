
#include "common.h"
#include "cnntools.h"

#include <sys/stat.h>//包含头文件。

int file_size(char* filename) {
    struct stat statbuf;
    int ret;
    ret = stat(filename,&statbuf);  //调用stat函数
    if(ret != 0) return -1;         //获取失败。
    return statbuf.st_size;         //返回文件大小。
}

void printStream(const CNN_PARA *cnnPara) {
    int i = 0, j = 0;
    uint8 *stream = cnnPara->stream;
    for(i = 0; i < cnnPara->height; i++) {
        for(j = 0; j < cnnPara->width; j++) {
            printf("%02d ", *stream++);
        }
        printf("\n");
    }
}

void printDStream(const CNN_PARA *cnnPara) {
    int i = 0, j = 0;
    uint8 *stream = cnnPara->dstream;
    for(i = 0; i < cnnPara->height; i++) {
        for(j = 0; j < cnnPara->width; j++) {
            printf("%02d ", *stream++);
        }
        printf("\n");
    }
}

void writeWithIndex(const CNN_PARA *cnnPara, int index) {
    
    char fname1[30] = { 0 };
    char fname2[30] = { 0 };
    char fname3[30] = { 0 };

    sprintf(&fname1, "./bin/t_stream_%d.bin", index);
    sprintf(&fname2, "./bin/t_dstream_%d.bin", index);
    sprintf(&fname3, "./bin/t_cnkner_%d.bin", index);

    int fd1 = open(&fname1, O_RDWR | O_CREAT, 0777);
    int fd2 = open(&fname2, O_RDWR | O_CREAT, 0777);
    int fd3 = open(&fname3, O_RDWR | O_CREAT, 0777);

    int wlen = 0;

    if(fd1 <= 0 || fd2 <= 0 || fd3 <= 0) {
        LOG_ERROR("write file error");
        return ;
    }

    wlen = write(fd1, cnnPara->stream, cnnPara->height*cnnPara->width);
    LOG_DEBUG("write stream wlen: %d", wlen);
    wlen = write(fd2, cnnPara->dstream, cnnPara->height*cnnPara->width);
    LOG_DEBUG("write dstream wlen: %d", wlen);
    wlen = write(fd3, cnnPara->cnnker, cnnPara->ckrlen[0]*cnnPara->ckrlen[0]);
    LOG_DEBUG("write cnnPara wlen: %d", wlen);

    close(fd1);
    close(fd2);
    close(fd3);
}

void writeWithCNNOne(const CNN_PARA *cnnPara, int times, int index) {
    
    char fname1[30] = { 0 };
    char fname2[30] = { 0 };
    char fname3[30] = { 0 };

    sprintf(&fname1, "./bin/t_stream_%d_%d.bin", times, index);
    sprintf(&fname2, "./bin/t_dstream_%d_%d.bin", times,index);
    sprintf(&fname3, "./bin/t_cnkner_%d_%d.bin", times, index);

    int fd1 = open(&fname1, O_RDWR | O_CREAT, 0777);
    int fd2 = open(&fname2, O_RDWR | O_CREAT, 0777);
    int fd3 = open(&fname3, O_RDWR | O_CREAT, 0777);

    int wlen = 0;

    if(fd1 <= 0 || fd2 <= 0 || fd3 <= 0) {
        LOG_ERROR("write file error");
        return ;
    }

    wlen = write(fd1, cnnPara->stream, cnnPara->height*cnnPara->width);
    LOG_DEBUG("write  stream wlen: %d->%d*%d", wlen, cnnPara->height, cnnPara->width);
    wlen = write(fd2, cnnPara->dstream, cnnPara->height*cnnPara->width);
    LOG_DEBUG("write dstream wlen: %d", wlen);
    int tindex    =  index;
    int offset = 0;
    for(tindex = 0; tindex < index; tindex++) {
        offset += cnnPara->ckrlen[tindex] * cnnPara->ckrlen[tindex];
    }
    wlen = write(fd3, cnnPara->cnnker + offset, cnnPara->ckrlen[index]*cnnPara->ckrlen[index]);
    LOG_DEBUG("write cnnPara wlen: %d", wlen);

    close(fd1);
    close(fd2);
    close(fd3);
}

void writeAll(const CNN_PARA *cnnPara) {
    int fd = open("./bin/t_stream.bin", O_RDWR | O_CREAT, 0777);
    int fd1 = open("./bin/t_dstream.bin", O_RDWR | O_CREAT, 0777);
    int fd2 = open("./bin/t_kstream.bin", O_RDWR | O_CREAT, 0777);

    int wlen = 0;

    if(fd <= 0 || fd1 <= 0 || fd2 <= 0) {
        LOG_ERROR("write file error");
        return ;
    }

    wlen = write(fd, cnnPara->stream, cnnPara->height*cnnPara->width);
    LOG_DEBUG("write stream wlen: %d", wlen);
    wlen = write(fd1, cnnPara->dstream, cnnPara->height*cnnPara->width);
    LOG_DEBUG("write dstream wlen: %d", wlen);
    wlen = write(fd2, cnnPara->cnnker, cnnPara->ckrlen[0]*cnnPara->ckrlen[0]);
    LOG_DEBUG("write dstream wlen: %d", wlen);

    close(fd);
    close(fd1);
    close(fd2);
}

void printCnnker(const CNN_PARA *cnnPara) {
    int i = 0, j = 0;
    uint8 *stream = cnnPara->cnnker;
    for(i = 0; i < cnnPara->ckrlen[0]; i++) {
        for(j = 0; j < cnnPara->ckrlen[0]; j++) {
            printf("%02d ", *stream++);
        }
        printf("\n");
    }
}