#include "common.h"
static uint8 __CUR_TIME_H[20]; 
static time_t timep;

uint8 *__GET_LOCAL_TIME() {
  time(&timep);
  struct tm *tm = localtime(&timep);
  sprintf(__CUR_TIME_H, "%04d_%02d_%02d %02d:%02d:%02d", tm->tm_year+ 1900, tm->tm_mon + 1, tm->tm_mday, tm->tm_hour, tm->tm_min, tm->tm_sec);
  return __CUR_TIME_H;
}