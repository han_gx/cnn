#include "common.h"
#include "cnntools.h"
#include "js_mm.h"
MODE_CALCULAT mode_type;

JSZT_CODE jszt_init(MODE_CALCULAT mode, int CPUS) {

    LOG_INFO("load pcie driver");
    mode_type = mode;
    int ret = 0;
    if(mode_type == CALCULAT_CPU) {
        if(MM_OK != (ret = js_mm_init(MM_MAX_TIME, MM_MAX_SIZE))) {
            return JS_ERROR_MALLOC;
        }
        if(JS_OK != (ret = init_CNN(CPUS, get_nprocs()))) {
            return ret;
        }
    }
    return JS_OK;
}

JSZT_CODE jszt_setReady() {
    return cnn_setReady();
}

JSZT_CODE jszt_setEnd() {
    return cnn_setEnd();
}

JSZT_CODE jszt_destroy() {
    LOG_INFO("system end !");
    js_mm_end();
    return JS_OK;
}

JSZT_CODE jszt_CNN(const CNN_PARA *cPara) {
    // LOG_INFO("jszt CNN start");
    uint8 *stream = cPara->stream;
    uint8 *cnnker = cPara->cnnker;
    JSZT_CODE errCode =  0 ;

    if(mode_type == CALCULAT_CPU) {
        //do something
        errCode = do_calculat_cpu(cPara);
    } else if(mode_type == CALCULAT_FPGA){
        errCode = do_calculat_fpga(cPara);
    } else {
        LOG_ERROR("dot support current mode type");
        return JS_FAILED;
    }
    return errCode;
}

JSZT_CODE do_calculat_fpga(const CNN_PARA *cPara) {
    return JS_OK;
}

JSZT_CODE do_calculat_cpu(const CNN_PARA *cPara) {
    return manage_CNN(cPara);
}

