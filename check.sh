#!/bin/bash
path=./bin/
# ./tcheck_x86.out ${path}t_stream_$1.bin ${path}t_cnkner_$1.bin ${path}t_dstream_$1.bin 1024 1024 2
# ./tcheck_x86.out ${path}t_stream_$1.bin ${path}t_cnkner_$1.bin ${path}t_dstream_$1.bin 60 60 1

cmd1=$(ls -l bin/t_stream_*.bin |grep "^-"|wc -l)

i=1
declare -A arr_code
arr_index=0
while [ $i -le $cmd1 ]
do
    echo "==>"$(printf "%02d" "$i")
    ./tcheck_x86.out ${path}t_stream_$i.bin ${path}t_cnkner_$i.bin ${path}t_dstream_$i.bin $1 $1 1

    if [ $? -eq "255" ]
        then 
        arr_code[$arr_index]=$(printf "%02d" "$i")
    fi

    let "i++"
    let "arr_index++"
done

echo ${arr_code[*]}